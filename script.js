var lista = document.getElementById('lista');

var pokemons = [
  {
    numero : "001",
    nome : "Bulbasaur",
    tipo : "Planta",
    descricao : "Verde",
    url : "https://vignette.wikia.nocookie.net/victoryroad/images/1/1a/FLArt_001.png/revision/latest/scale-to-width-down/240?cb:20160820204910"
  },{
    numero : "002" ,
    nome : "Ivysaur",
    tipo : "Planta",
    descricao : "Verde",
    url : "https://vignette.wikia.nocookie.net/victoryroad/images/c/c3/FLArt_002.png/revision/latest/scale-to-width-down/230?cb:20160820205223"
  },{
    numero : "003",
    nome : "Venusaur",
    tipo : "Planta",
    descricao : "Verde",
    url : "https://vignette.wikia.nocookie.net/victoryroad/images/b/b5/FLArt_003.png/revision/latest?cb:20110330162201"
  },{
    numero : "004",
    nome : "Charmander",
    tipo : "Fogo",
    descricao : "Laranja",
    url : "https://vignette.wikia.nocookie.net/victoryroad/images/b/b8/FLArt_004.png/revision/latest/scale-to-width-down/230?cb:20160820205635"
  },{
    numero : "005",
    nome : "Charmeleon",
    tipo : "Fogo",
    descricao : "Laranja",
    url : "https://vignette.wikia.nocookie.net/victoryroad/images/b/b5/FLArt_005.png/revision/latest/scale-to-width-down/230?cb:20160820210122"
  },{
    numero : "006",
    nome : "Charizard",
    tipo : "Fogo",
    descricao : "Laranja",
    url : "https://vignette.wikia.nocookie.net/victoryroad/images/b/b4/FRLGArt_Charizard.png/revision/latest/scale-to-width-down/265?cb:20170821013841"
  },{
    numero : "007",
    nome : "Squirtle",
    tipo : "Agua",
    descricao : "Azul",
    url : "https://vignette.wikia.nocookie.net/victoryroad/images/f/f0/FLArt_007.png/revision/latest/scale-to-width-down/230?cb:20160820210328"
  },{
    numero : "008",
    nome : "Wartortle",
    tipo : "Agua",
    descricao : "Azul",
    url : "https://vignette.wikia.nocookie.net/victoryroad/images/f/fd/FLArt_008.png/revision/latest/scale-to-width-down/230?cb:20160820210620"
  },{
    numero : "009",
    nome : "Blastoise",
    tipo : "Agua",
    descricao : "Azul",
    url : "https://vignette.wikia.nocookie.net/victoryroad/images/4/4d/FLArt_009.png/revision/latest/scale-to-width-down/240?cb:20160820212103"
  },{
    numero : "010",
    nome : "Caterpie",
    tipo : "Inseto",
    descricao : "Verde",
    url : "https://vignette.wikia.nocookie.net/victoryroad/images/5/50/FL_Caterpie.png/revision/latest/scale-to-width-down/198?cb=20110401024545"
  }
]

var edit = null;

function adicionar(event){
  event.preventDefault();
  if(edit == null){
    pokemons.push({
      numero : document.getElementById('numero').value,
      nome : document.getElementById('nome').value,
      tipo : document.getElementById('tipo').value,
      descricao : document.getElementById('descricao').value,
      url : document.getElementById('url').value
    })
  }
  else{
    pokemon = pokemons.filter(item => item.numero == edit)[0];
    pokemon.numero = document.getElementById('numero').value;
    pokemon.nome = document.getElementById('nome').value;
    pokemon.tipo = document.getElementById('tipo').value;
    pokemon.descricao = document.getElementById('descricao').value;
    pokemon.url = document.getElementById('url').value;
    edit = null;
  }
  document.getElementById('numero').value =  "";
  document.getElementById('nome').value = "";
  document.getElementById('tipo').value = "";
  document.getElementById('descricao').value = "";
  document.getElementById('url').value = "";
  atualizar(pokemons);
}

function atualizar(listapokemons){
  lista.innerHTML = "";
  pokemons = listapokemons;
  listapokemons.forEach((item) => {

    var div = document.createElement("div");
    var numero = document.createElement("p");
    var nome = document.createElement("p");
    var tipo = document.createElement("p");
    var descricao = document.createElement("p");
    var img = document.createElement("img");
    var br = document.createElement("br");
    var br1 = document.createElement("br");
    var br2 = document.createElement("br");
    var br3 = document.createElement("br");
    var apagar = document.createElement("a");
    var editar = document.createElement("a");
    var hr = document.createElement("hr");

    numero.innerText = "Numero: "+item.numero;
    nome.innerText = "Nome: "+item.nome;
    tipo.innerText = "Tipo: "+item.tipo;
    descricao.innerText = "Descrição: "+item.descricao;
    img.src = item.url;
    img.setAttribute("style", "width:200px; height:200px");
    apagar.innerText = "APAGAR";
    apagar.id = item.numero;
    editar.innerText = "EDITAR";
    editar.id = item.numero;


    apagar.onclick = function(){
      listpokemons = pokemons.filter(item => item.numero != apagar.id);
      console.log(listpokemons);
      atualizar(listpokemons);
    };

    editar.onclick = function(){
      pokemon = pokemons.filter(item => item.numero == editar.id);
      document.getElementById('numero').value = pokemon[0].numero;
      document.getElementById('nome').value = pokemon[0].nome;
      document.getElementById('tipo').value = pokemon[0].tipo;
      document.getElementById('descricao').value = pokemon[0].descricao;
      document.getElementById('url').value = pokemon[0].url;
      edit = document.getElementById('numero').value;
      document.getElementById('btn').value = "Salvar";
    }

    div.appendChild(numero);
    div.appendChild(nome);
    div.appendChild(tipo);
    div.appendChild(descricao);
    descricao.appendChild(br);
    descricao.appendChild(img);
    div.appendChild(br1);
    div.appendChild(apagar);
    div.appendChild(editar);
    div.appendChild(br2);

    div.appendChild(br3);
    div.appendChild(hr);
    lista.appendChild(div);
  });
  document.getElementById('btn').value = "Adicionar";

}

atualizar(pokemons);

function mostrarItens (){
  document.getElementsByTagName("html")[0].style.overflowY = "hidden";
  document.getElementById("showCase").removeAttribute("hidden");
}

function fechar (){
  document.getElementsByTagName("html")[0].style.overflowY = "auto";
  document.getElementById("showCase").hidden = true;
}
